package ictgradschool.industry.lab_concurrency.ex05;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab_concurrency.examples.example03.PrimeFactors;

public class PrimeFactorsCommandLine {

    protected class Abort implements Runnable{
        Thread taskInOtherThread;

        public Abort(Thread otherThread){
            taskInOtherThread = otherThread;
        }

        @Override
        public void run(){
            System.out.println("Type 'a' to abort");
            if(Keyboard.readInput().toLowerCase().startsWith("a")){
                System.out.println("Tried to interrupt");
                //taskInOtherThread.currentState=PrimeFactorsTask.TaskState.Aborted;
                taskInOtherThread.interrupt();
            }

            }
        }



public void start(){
    System.out.println("Welcome to the Prime Factors Task");
    System.out.println("Please enter a number to factorise");
    long userNumber = Long.parseLong(Keyboard.readInput());

    PrimeFactorsTask ourTask = new PrimeFactorsTask(userNumber);
    Thread ourThread = new Thread(ourTask);
    ourThread.start();

    Abort checkToStop = new Abort(ourThread);
    Thread abortThread = new Thread(checkToStop);
    abortThread.start();

    while(ourTask.getState()== PrimeFactorsTask.TaskState.Initialized){}
    ourThread.interrupt();
    abortThread.interrupt();

    try{
    ourThread.join();
    abortThread.join();}catch(InterruptedException e){e.getMessage(); e.getStackTrace();}

    System.out.println("factors include:");
    System.out.println(ourTask.getPrimeFactors().toString());
    /*for(long x: ourTask.getPrimeFactors()){
        System.out.println(x);
    }*/
}

    public static void main(String[] args) {
        PrimeFactorsCommandLine program = new PrimeFactorsCommandLine();
        program.start();
    }
}
