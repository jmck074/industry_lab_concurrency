package ictgradschool.industry.lab_concurrency.ex05;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsTask implements Runnable{



    public enum TaskState{Initialized, Completed, Aborted}
    Long n;
    List<Long> listOfFactors;
    TaskState currentState;

    public PrimeFactorsTask(long N){
    n = N;
    listOfFactors = new ArrayList<Long>() ;
    currentState = TaskState.Initialized;

    }


    @Override
    public void run() {

        for (long factor = 2; (factor*factor <= n)&&(!Thread.interrupted()); factor++) {

            // if factor is a factor of n, repeatedly divide it out
            while (n % factor == 0) {
                System.out.print(factor + " ");
                listOfFactors.add(factor);
                n = n / factor;
            }
        }

        // if biggest factor occurs only once, n > 1
        if (n > 1) {System.out.println(n);
        listOfFactors.add(n);}
        else       System.out.println();
        if(currentState!=TaskState.Aborted){
        System.out.println("About to change the state to complete");}
        currentState=TaskState.Completed;
        System.out.println("State has changed");}


    public long n(){return n;}

public List<Long> getPrimeFactors() throws IllegalStateException{
        if(currentState==TaskState.Completed){
            return listOfFactors;
        }
        throw new IllegalStateException();
}

public TaskState getState(){
        return currentState;
}
public void setTaskState(TaskState state){
        currentState=state;
}

}

