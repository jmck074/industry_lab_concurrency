package ictgradschool.industry.lab_concurrency.ex04;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;


public class ConcurrentBankingApp {
    //created a blocking queue object with a capacity of ten
    //pdf suggests to use final, this way it is accessible to consumer threads even though we do a lot of the work in producer class?

    ArrayBlockingQueue<Transaction> blockingQueueObject = new ArrayBlockingQueue<>(10);
    BankAccount account = new BankAccount();

    protected class Consumer implements Runnable {
        private ArrayBlockingQueue<Transaction> queue;

        protected Consumer(ArrayBlockingQueue<Transaction> bQO) {
            queue = bQO;
        }

        @Override
        public void  run() {
            try {
                while (true) {
                    System.out.println("Consumer step");
                    System.out.println(queue.size());
                    Transaction t = queue.take();
                    switch (t._type) {
                        case Deposit:
                            account.deposit(t._amountInCents);
                            System.out.println("Depositing "+t._amountInCents);
                            break;
                        case Withdraw:
                            account.withdraw(t._amountInCents);
                            System.out.println("Withdrawing "+t._amountInCents);
                            break;
                    }
                }
            } catch (InterruptedException e) {
                e.getMessage();
                e.getStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        new ConcurrentBankingApp().start();
    }


    public void start() {


        //create three threads
        Thread theProducer = new Thread(new Runnable() {
            @Override
            public void run() {
                List<Transaction> listOfTransactions = TransactionGenerator.readDataFile();
                for (Transaction t : listOfTransactions) {
                    try {
                        blockingQueueObject.put(t);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        });


        Consumer consumer1 = new Consumer(blockingQueueObject);
        Consumer consumer2 = new Consumer(blockingQueueObject);
        Thread consumer1Thread = new Thread(consumer1);
        Thread consumer2Thread = new Thread(consumer2);

        theProducer.start();
        consumer1Thread.start();
        consumer2Thread.start();

        //theProducer.interrupt();

        try {
            theProducer.join();

        }catch(InterruptedException e){e.getMessage(); e.getStackTrace();}
        while(!blockingQueueObject.isEmpty()){

        }
        consumer1Thread.interrupt();
        consumer2Thread.interrupt();

        try{
            consumer1Thread.join();
            consumer2Thread.join();
        }catch(InterruptedException e){e.getMessage();}


        System.out.println("Final balance: " + account.getFormattedBalance());

        //Acquire transactions to process

        //ArrayBlockingQueue<Transaction> transactions = TransactionGenerator.readDataFile();


        //Create bank account object to operate on


        //For each transaction, apply it to the bank account instance


    }
}

