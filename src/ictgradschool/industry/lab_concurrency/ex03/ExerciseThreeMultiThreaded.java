package ictgradschool.industry.lab_concurrency.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples){
        // TODO Implement this.
        double allNumInsideCircle = 0;
        int NUMBER_OF_THREADS = 100000;

         List<Thread> myThreads = new ArrayList<>();
         Vector<Double> eachCount = new Vector<>();
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {

            Thread t = new Thread(new Runnable() {
                long numInsideCircle = 0;
                @Override
                public void run() {
                    ThreadLocalRandom tlr = ThreadLocalRandom.current();
                    for (long j = 0; j < (numSamples / NUMBER_OF_THREADS); j++) {
                        double x = tlr.nextDouble();
                        double y = tlr.nextDouble();
                        if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                            numInsideCircle++;
                        }
                    }
                    double estimatedPI = 4.0 * (double) numInsideCircle / ((double)numSamples/(double)NUMBER_OF_THREADS);
                    //System.out.println(estimatedPI);
                    eachCount.add(estimatedPI);

                }

            });

            myThreads.add(t);
        }
        for (Thread t : myThreads) {
            t.start();
        }
        for (Thread t : myThreads) {
            try{
            t.join();}
            catch (InterruptedException e){
                e.getMessage();
            }
            //allNumInsideCircle = allNumInsideCircle + t.numInsideCircle;

        }
        for(double item: eachCount){
            //System.out.println(item);
            allNumInsideCircle=allNumInsideCircle+item;
        }
        double estimatedPI = allNumInsideCircle/NUMBER_OF_THREADS;



        return estimatedPI;
    }

    /**
     * Program entry point.
     */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
