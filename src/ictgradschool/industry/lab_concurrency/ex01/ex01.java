package ictgradschool.industry.lab_concurrency.ex01;

import ictgradschool.Keyboard;

public class ex01 {

    private void start(){
         Runnable myRunnable = new Runnable(){
             @Override
             public void run(){
                for(int i=0; !Thread.currentThread().isInterrupted()&&i<1000001;i++){
                    System.out.println(i);
                }
             }
         };
         //myRunnable.run();
        Thread someThread = new Thread(myRunnable);
        someThread.start();
        String a = Keyboard.readInput();

        someThread.interrupt();
    }

    public static void main(String[] args) {
        ex01 instance = new ex01();
        instance.start();


    }
}
